package cz.cvut.fit.tjv.inventory.cv8_final;

import cz.cvut.fit.tjv.inventory.business.CharacterService;
import cz.cvut.fit.tjv.inventory.data.entities.Character;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collection;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/characters")
public class CharacterController {
    private final CharacterService characterService;

    public CharacterController(CharacterService characterService) {
        this.characterService = characterService;
    }

    @PostMapping
    public void create(@RequestBody Character data){
        characterService.create(data);
    }

    @GetMapping("/{id}")
    public Character readOne(@PathVariable String id){
        Optional<Character> optionalCharacter = characterService.readById(id);
        return optionalCharacter.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    // GET /characters?page=3&size=120
    @GetMapping
    public Page<Character> readAll(@RequestParam("0") int page,@RequestParam("10") int size){
        return characterService.readAll(PageRequest.of(page, size));
    }

    @PutMapping("/{id}")
    public void update(@PathVariable String id, @RequestBody Character newData){
         characterService.update(newData);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable String id){
        characterService.deleteById(id);
    }
}
