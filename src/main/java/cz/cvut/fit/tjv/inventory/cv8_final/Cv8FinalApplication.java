package cz.cvut.fit.tjv.inventory.cv8_final;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan("cz.cvut.tjv.fit.inventory.data.entities")
@EnableJpaRepositories("cz.cvut.fit.tjv.inventory.data")
@ComponentScan("cz.cvut.fit.tjv.inventory.business")
public class Cv8FinalApplication {

    public static void main(String[] args) {
        SpringApplication.run(Cv8FinalApplication.class, args);
    }

}
